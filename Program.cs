﻿using csharp_first_sample;
using MongoDB.Driver;

var mongoUrl = new MongoUrl("mongodb://172.31.100.100/sample");
var client = new MongoClient(mongoUrl);
var db = client.GetDatabase(mongoUrl.DatabaseName);
var coll = db.GetCollection<Person>("csharp-people");
await coll.InsertOneAsync(new Person(19, "Pietro", "Martinelli"));