namespace csharp_first_sample;

public record Person(int Id, string FirstName, string LastName);